const express = require('express')
const app = express()

const bodyParser = require('body-parser')

const config = require('./config/app')

const mongoose = require('mongoose')
const User = require('./models/User')

mongoose.connect(
    config.db.connection,
    {
        useNewUrlParser: true
    }
).then(() => {
    console.log("MongoDB Connected")
}).catch(err => {
    console.log(err)
})

const port = process.env.port || 3000

app.use(bodyParser.json())

app.use('/api', require('./routes/api'))

app.listen(port, (err) => {
    if (err) {
        console.error(err)
        return
    }
    console.log(`Academy app listening on port ${port}`)
})