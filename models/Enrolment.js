const mongoose = require('mongoose')

const EnrolmentSchema = mongoose.Schema({
    user: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: true
    },
    course: {
        type: mongoose.Types.ObjectId,
        ref: 'Course',
        required: true
    },
    status: {
        type: String,
        enum: ['new', 'active', 'passed', 'failed'],
        default: 'new'
    },
    rate: {
        type: Number,
        default: 0
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
    created_at: {
        type: Date,
        default: Date.now
    }
})

const Enrolment = mongoose.model('Enrolment', EnrolmentSchema)

module.exports = Enrolment