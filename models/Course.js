const mongoose = require('mongoose')

const CourseSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    description: {
        type: String, 
        required: true
    },
    thumbnail: {
        type: String,
        required: true
    },
    published: {
        type: Boolean,
        default: false
    },
    categories: {
        type: Array,
        default: [],
    },
    tags: {
        type: Array,
        default: []
    },
    created_at: {
        type: Date,
        default: Date.now
    }
})

const Course = mongoose.model('Course', CourseSchema)

module.exports = Course