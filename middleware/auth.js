const jwt = require('jsonwebtoken')
const config = require('../config/app')

var valid = (str) => {
    return str !== undefined && str !== ""
}

const auth = function(req, res, next) {
    var authHeader = req.headers.authorization

    if (!valid(authHeader)) {
        return res.status(401).json({
            message: 'Missing authorization token'
        })
    }

    var parts = authHeader.split(' ')

    if (parts[0] !== 'Bearer') {
        return res.status(401).json({
            message: 'Invalid token'
        })
    }

    var token = parts[1]

    jwt.verify(token, config.auth.secret, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                message: 'Invalid token'
            })
        }

        // TODO: Check expire date
        
        req.user = decoded
        next()
    })
}

module.exports = auth