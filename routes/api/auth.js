const express = require('express')
const router = express.Router()

const User = require('../../models/User')

var valid = (str) => {
    return str !== undefined && str !== ""
}

router.post('/register', (req, res) => {
    if (!valid(req.body.username)) {
        return res.status(422).json({
            message: 'Username is required'
        })
    }
    if (!valid(req.body.email)) {
        return res.status(422).json({
            message: 'Email is required'
        })
    }
    if (!valid(req.body.password)) {
        return res.status(422).json({
            message: 'Password is required'
        })
    }

    var user = new User({
        username: req.body.username,
        email: req.body.email,
    })

    user.setPassword(req.body.password)

    return user.save()
        .then(() => {
            res.status(200).json({
                user: user.toAuthJSON()
            })
        })
        .catch(err => {
            return res.status(500).json({
                message: 'Server error'
            })
        })
})

router.post('/login', (req, res) => {
    var username = req.body.username
    var password = req.body.password

    if (!valid(username)) {
        return res.status(422).json({
            message: 'Username is required'
        })
    }
    if (!valid(password)) {
        return res.status(422).json({
            message: 'Password is required'
        })
    }

    User.findOne({ username })
        .then(user => {
            if (!user) {
                return res.status(400).json({
                    message: 'Invalid username'
                })
            }
            if(!user.validatePassword(password)) {
                return res.status(400).json({
                    message: 'Invalid password'
                })
            }
            return res.status(200).json({
                user: user.toAuthJSON()
            })
        })
        .catch(err => {
            return res.status(500).json({
                message: 'Server error'
            })
        })
})

module.exports = router