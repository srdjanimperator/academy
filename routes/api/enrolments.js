const express = require('express')
const router = express.Router()

const Enrolment = require('../../models/Enrolment')

router.post('/', (req, res) => {
    // TODO: Add validation!

    let users = req.body.users
    let course = req.body.course

    if(!users) {
        users = [
            req.user.id
        ]
    }

    var enrolments = users.map(u => new Enrolment({
        user: u,
        course: course
    }))

    Enrolment.insertMany(enrolments)
        .then(docs => {
            return res.status(201).json({
                enrolments: docs
            })
        })
        .catch(err => {
            consosle.log(err)
            return res.status(500).json({
                message: 'Server error'
            })
        })
})

module.exports = router