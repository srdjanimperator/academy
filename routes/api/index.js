const express = require('express')
const router = express.Router()

const authMiddleware = require('../../middleware/auth')

const auth = require('./auth')
const users = require('./users')
const courses = require('./courses')
const categories = require('./categories')
const enrolments = require('./enrolments')

router.use('/auth', auth)
router.use('/', authMiddleware)
router.use('/users', users)
router.use('/courses', courses)
router.use('/categories', categories)
router.use('/enrolments', enrolments)

module.exports = router