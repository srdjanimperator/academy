const express = require('express')
const router = express.Router()

const User = require('../../models/User')

router.get('/', (req, res) => {

    // TODO: Implement query and pagination
    
    User.find({}, (err, users) => {
        if (err) {
            return res.status(500).json({
                message: 'Server error'
            })
        }
        return res.status(200).json({
            collection: users.map(u => u.forAPI()),
            size: users.length
        })
    })
})

router.get('/:id', (req, res) => {
    User.findOne({_id: req.params.id}, (err, user) => {
        if (err) {
            return res.status(500).json({
                message: 'Server error'
            })
        }
        if (!user) {
            return res.status(404).json({
                message: 'User not found'
            })
        }
        return res.status(200).json({
            user: user.forAPI()
        })
    })
})

module.exports = router