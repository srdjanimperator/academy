const express = require('express')
const router = express.Router()

const Category = require('../../models/Category')

router.get('/', (req, res) => {

    // TODO: Implement query and pagination

    let filter = {}
    if (req.query.filter) {
        filter = JSON.parse(req.query.filter)
    }
    
    Category.find(filter, (err, categories) => {
        if (err) {
            return res.status(500).json({
                message: 'Server error'
            })
        }
        return res.status(200).json({
            collection: categories,
            size: categories.length
        })
    })
})

router.post('/', (req, res) => {

    // TODO : Request validation

    var category = new Category({
        name: req.body.name,
        description: req.body.description || 'No description available',
        thumbnail: req.body.thumbnail || 'default.png'
    })

    return category.save()
        .then(() => {
            return res.status(201).json({
                category: category
            })
        })
        .catch(err => {
            return res.status(500).json({
                message: 'Server error'
            })
        })
})

module.exports = router