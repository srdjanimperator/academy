const express = require('express')
const router = express.Router()

const Course = require('../../models/Course')

router.get('/', (req, res) => {

    // TODO: Implement query and pagination

    let filter = {}
    if (req.query.filter) {
        filter = JSON.parse(req.query.filter) 
    }

    Course.find(filter, (err, courses) => {
        if (err) {
            return res.status(500).json({
                message: 'Server error'
            })
        }
        return res.status(200).json({
            collection: courses,
            size: courses.length
        })
    })
})

router.post('/', (req, res) => {
    
    // TODO : Request validation

    var course = new Course ({
        name: req.body.name,
        author: req.user.id,
        description: req.body.description,
        thumbnail: req.body.thumbnail,
        tags: req.body.tags
    })

    return course.save()
        .then(() => {
            res.status(201).json({
                course: course
            })
        })
        .catch(err => {
            return res.status(500).json({
                message: 'Server error'
            })
        })
})

module.exports = router