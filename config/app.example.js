module.exports = {
    auth: {
        secret: '<SECRET>'
    },
    db: {
        connection: '<MONGODB-CONNECTION-STRING>',
    },
}